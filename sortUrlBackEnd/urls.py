from django.conf.urls import url
from sorturl import views

urlpatterns = [
    url(r'^$', views.Index),
    url(r'^sort$', views.Sort),
    url(r'^(.+)', views.GetUrl),
]
