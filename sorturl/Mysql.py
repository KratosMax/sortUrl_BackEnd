import pymysql
from DBUtils.PooledDB import PooledDB
from sortUrlBackEnd import settings

POOL = PooledDB(
    creator=pymysql,
    maxconnections=6,
    mincached=2,
    maxcached=5,
    maxshared=1,
    blocking=True,
    maxusage=None,
    setsession=[],
    ping=1,
    host=settings.mysql['ip'],
    port=3306,
    user=settings.mysql['user'],
    password=settings.mysql['passwd'],
    database=settings.mysql['database'],
    charset='utf8'
)


def MysqlPool(sql, num):
    # 执行sql
    conn = POOL.connection()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql)
    # 获取单条数据
    if num == 1:
        result = cursor.fetchone()
    elif num == 99:
        result = cursor.fetchall()
    elif num == 0:
        conn.commit()
        result = "执行成功"
    else:
        result = cursor.fetchmany(num)
    conn.close()
    cursor.close()
    return result
