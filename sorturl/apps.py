from django.apps import AppConfig


class SorturlConfig(AppConfig):
    name = 'sorturl'
