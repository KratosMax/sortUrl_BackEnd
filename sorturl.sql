/*
 Navicat MySQL Data Transfer

 Source Server         : 本地短域名
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3306
 Source Schema         : sorturl

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 27/03/2020 16:24:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for urls
-- ----------------------------
DROP TABLE IF EXISTS `urls`;
CREATE TABLE `urls`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hashurl` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fullurl` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `creattime` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `savetime` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `hashurl`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
